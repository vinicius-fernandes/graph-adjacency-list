/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Vertex.h
 * Author: vinicius
 *
 * Created on 6 de Julho de 2018, 20:01
 */

#ifndef VERTEX_H
#define VERTEX_H

#include <string>

using namespace std;

class Vertex {
private:

    int key; // Vertex key
    string label; // Vertex label

public:

    /**
     * default constructor
     */
    Vertex();

    /**
     * 
     * @param key
     * @param label
     */
    Vertex(int key, string label);

    /**
     * default destructor
     */
    virtual ~Vertex();

    /**
     * set the vertex label
     * @param label
     */
    void SetLabel(string label);

    /**
     * return the vertex label
     * @return 
     */
    string GetLabel() const;

    /**
     * set the vertex key
     * @param key
     */
    void SetKey(int key);

    /**
     * return the vertex key
     * @return 
     */
    int GetKey() const;

};

#endif /* VERTEX_H */

