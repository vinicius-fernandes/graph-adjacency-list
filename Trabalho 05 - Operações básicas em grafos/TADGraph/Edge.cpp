/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Edge.cpp
 * Author: vinicius
 * 
 * Created on 6 de Julho de 2018, 20:02
 */

#include <stddef.h>

#include "Edge.h"

/**
 * 
 */
Edge::Edge() {
    this->directed = false;
    this->cost = 0;
    this->vertex = NULL;
}

/**
 * 
 * @param vertex
 * @param dir
 * @param cost
 */
Edge::Edge(Vertex *vertex, bool dir, int cost) {
    this->directed = dir;
    this->cost = cost;
    this->vertex = vertex;
}

/**
 * 
 */
Edge::~Edge() {
    delete this->vertex;
}

/**
 * 
 * @param vertex
 */
void Edge::SetVertex(Vertex* vertex) {
    this->vertex = vertex;
}

/**
 * 
 * @return 
 */
Vertex* Edge::GetVertex() const {
    return vertex;
}

/**
 * 
 * @param directed
 */
void Edge::SetDirected(bool directed) {
    this->directed = directed;
}

/**
 * 
 * @return 
 */
bool Edge::IsDirected() const {
    return directed;
}

/**
 * 
 * @param cost
 */
void Edge::SetCost(int cost) {
    this->cost = cost;
}

/**
 * 
 * @return 
 */
int Edge::GetCost() const {
    return cost;
}

