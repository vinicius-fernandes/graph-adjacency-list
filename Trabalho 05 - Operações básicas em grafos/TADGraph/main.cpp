/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vinicius
 *
 * Created on 6 de Julho de 2018, 19:59
 */

#include <iostream>
#include <cstdlib>

#include "GraphAL.h"

using namespace std;

// menu method escope
int menu();

// global variables
Edge *edge_aux = NULL;
Vertex *vertex_aux = NULL;
int option;
char flag;
string label;
int u, v;
int nVertices = 5;
list<Vertex*> vertices;
list<Edge*> edges;

int main() {
    system("clear");

    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐         GRAPH CONSTRUCTOR       ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ NUMBER OF VERTICES: ";
    cin >> nVertices;

    if (nVertices > 0) {
        system("clear");

        GraphAL graph(nVertices);

        // Test Case
        graph.InsertEdge(1, 2, false, 5);
        graph.InsertEdge(1, 3, true, 2);
        graph.InsertEdge(1, 5, true, 10);
        graph.InsertEdge(5, 4, false, 0);
        graph.InsertEdge(4, 3, true, 8);

        while (true) {

            option = menu(); // option chosen from the menu by the user
            system("clear");

            switch (option) {

                case 1:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐            GetNVertices         ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ NUMBER OF VERTICES: " << graph.GetNVertices() << endl;
                    break;


                case 2:
                    int cost;
                    char dir;

                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐      InsertEdge(u, v, dir, c)   ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ ENTER THE VERTEX: ";
                    cin >> u;
                    cout << "\n ➸ ENTER THE ADJACENT VERTEX: ";
                    cin >> v;
                    cout << "\n ➸ ENTER THE COST OF THE EDGE: ";
                    cin >> cost;
                    cout << "\n ➸ IS THIS EDGE DIRECTED OR NOT ? (y / n): ";
                    cin >> dir;

                    if (!graph.GetVertexLabel(v).empty() && !graph.GetVertexLabel(u).empty()) {

                        if (dir == 'y' || dir == 'Y')
                            graph.InsertEdge(u, v, true, cost);
                        else
                            graph.InsertEdge(u, v, false, cost);

                        cout << "\n\n ➸ INSERT: Edge inserted successfully" << endl;
                    } else
                        cout << "\n\n ➸ VERTEX u OR v NOT FOUND";
                    break;

                case 3:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐           HasEdge(u, v)         ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ ENTER THE VERTEX: ";
                    cin >> u;
                    cout << "\n ➸ ENTER THE ADJACENT VERTEX: ";
                    cin >> v;

                    if (!graph.GetVertexLabel(v).empty() && !graph.GetVertexLabel(u).empty()) {

                        if (graph.HasEdge(u, v))
                            cout << "\n ➸ HasEdge: TRUE";
                        else
                            cout << "\n ➸ HasEdge: FALSE";
                    } else
                        cout << "\n\n ➸ VERTEX u OR v NOT FOUND";
                    break;

                case 4:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐          IsDirected(u, v)       ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ ENTER THE VERTEX: ";
                    cin >> u;
                    cout << "\n ➸ ENTER THE ADJACENT VERTEX: ";
                    cin >> v;

                    if (!graph.GetVertexLabel(v).empty() && !graph.GetVertexLabel(u).empty()) {

                        if (graph.IsDirected(u, v))
                            cout << "\n ➸ IsDirected: TRUE";
                        else
                            cout << "\n ➸ IsDirected: FALSE";
                    } else
                        cout << "\n\n ➸ VERTEX u OR v NOT FOUND";
                    break;

                case 5:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐         IsEdgeValued(u, v)      ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ ENTER THE VERTEX: ";
                    cin >> u;
                    cout << "\n ➸ ENTER THE ADJACENT VERTEX: ";
                    cin >> v;

                    if (!graph.GetVertexLabel(v).empty() && !graph.GetVertexLabel(u).empty()) {

                        if (graph.IsEdgeValued(u, v))
                            cout << "\n ➸ IsEdgeValued: TRUE";
                        else
                            cout << "\n ➸ IsEdgeValued: FALSE";
                    } else
                        cout << "\n\n ➸ VERTEX u OR v NOT FOUND";
                    break;

                case 6:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐         GetEdgeValue(u, v)      ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ ENTER THE VERTEX: ";
                    cin >> u;
                    cout << "\n ➸ ENTER THE ADJACENT VERTEX: ";
                    cin >> v;

                    if (!graph.GetVertexLabel(v).empty() && !graph.GetVertexLabel(u).empty())
                        cout << "\n\n ➸ EDGE OF COST: " << graph.GetEdgeValue(u, v);
                    else
                        cout << "\n\n ➸ VERTEX u OR v NOT FOUND";
                    break;

                case 7:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐             GetNEdges           ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ GetNEdges: " << graph.GetNEdges() << endl;
                    break;

                case 8:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐          RemoveEdge(u, v)       ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ ENTER THE VERTEX: ";
                    cin >> u;
                    cout << "\n ➸ ENTER THE ADJACENT VERTEX: ";
                    cin >> v;

                    if (!graph.GetVertexLabel(v).empty() && !graph.GetVertexLabel(u).empty()) {

                        edge_aux = graph.RemoveEdge(u, v);

                        if (edge_aux != NULL)
                            cout << "\n\n ➸ RemoveEdge: Edge successfully removed";
                        else
                            cout << "\n\n RemoveEdge: Failed to remove edge";
                    } else
                        cout << "\n\n ➸ VERTEX u OR v NOT FOUND";
                    break;

                case 9:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐        GetAdjacencyVertex(v)    ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "\n ➸ ENTER THE VERTEX: ";
                    cin >> v;
                    cout << "\n\n ➸ ADJACENT LIST: [" << v << "]" << endl;

                    if (!graph.GetVertexLabel(v).empty()) {

                        vertices = graph.GetAdjacencyVertex(v);

                        if (!vertices.empty()) {

                            cout << "\t";
                            for (list<Vertex*>::iterator it = vertices.begin(); it != vertices.end(); ++it)
                                cout << "[" << (*it)->GetLabel() << "] -> ";
                        } else
                            cout << "\t ➸ EMPTY";
                    } else
                        cout << "\t ➸ VERTEX NOT FOUND";
                    break;

                case 10:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐         GetIncidentEdges(v)     ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "\n ➸ ENTER THE VERTEX: ";
                    cin >> v;
                    cout << "\n\n ➸ EDGES LIST: [" << v << "]" << endl;

                    if (!graph.GetVertexLabel(v).empty()) {

                        vertex_aux = graph.GetVertex(v);
                        edges = graph.GetIncidentEdges(v);

                        if (!edges.empty()) {

                            for (list<Edge*>::iterator it = edges.begin(); it != edges.end(); ++it) {

                                Edge *edge = (*it);

                                cout << "\n\t { ";

                                cout << "(" << vertex_aux->GetLabel() << ",";
                                cout << edge->GetVertex()->GetLabel() << ")";

                                cout << ", cost: " << edge->GetCost();

                                cout << ", directed: ";
                                if (edge->IsDirected())
                                    cout << "TRUE";
                                else
                                    cout << "FALSE";
                                cout << " }";
                            }
                        } else
                            cout << "\t ➸ EMPTY";
                    } else
                        cout << "\t ➸ VERTEX NOT FOUND";
                    break;

                case 11:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐      SetVertexLabel(v, label)   ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "\n ➸ ENTER THE VERTEX: ";
                    cin >> v;
                    cout << "\n ➸ ENTER THE LABEL: ";
                    cin >> label;

                    if (!graph.GetVertexLabel(v).empty()) {

                        graph.SetVertexLabel(v, label);
                        cout << "\n ➸ SetVertexLabel: Label changed successfully";
                    } else
                        cout << "\n\n ➸ VERTEX NOT FOUND";
                    break;

                case 12:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐         GetVertexLabel(v)       ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "\n ➸ ENTER THE VERTEX: ";
                    cin >> v;

                    if (!graph.GetVertexLabel(v).empty()) {

                        label = graph.GetVertexLabel(v);
                        cout << "\n ➸ GetVertexLabel: Label = " << label;

                    } else
                        cout << "\n\n ➸ VERTEX NOT FOUND";
                    break;

                case 13:
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << "▐             PrintGraph          ▐" << endl;
                    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                    cout << " ➸ PrintGraph: ";
                    graph.PrintGraph();
                    break;

                case 0:
                    exit(0);
                    break;

                default:
                    cout << "\n ➸ Invalid option. PLease choose a valid option! ( 1 - 10 )" << endl;
                    break;
            }

            cout << "\n\n ➸ Continue? (y / n):  ";
            cin >> flag;

            if (flag == 'n' || flag == 'N')
                exit(EXIT_SUCCESS);
        }

    } else {
        cout << "\n ➸ FAILED: SIZE MUST BE OVER 0 (> 0)" << endl;
        exit(0);
    }

    cout << endl;
    return 0;
}

/**
 * @return which function of the RedBlackTree the user wants to call.
 */
int menu() {
    system("clear");

    int option = 0;

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                     GRAPH - LIST ADJACENT                      ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << "▐\t" << "[  1  ] ➸ GetNVertices     [  8  ] ➸ RemoveEdge          ▐" << endl;
    cout << "▐\t" << "[  2  ] ➸ InsertEdge       [  9  ] ➸ GetAdjacencyVertex  ▐" << endl;
    cout << "▐\t" << "[  3  ] ➸ HasEdge          [ 10  ] ➸ GetIncidentEdges    ▐" << endl;
    cout << "▐\t" << "[  4  ] ➸ IsDirected       [ 11  ] ➸ SetVertexLabel      ▐" << endl;
    cout << "▐\t" << "[  5  ] ➸ IsEdgeValued     [ 12  ] ➸ GetVertexLabel      ▐" << endl;
    cout << "▐\t" << "[  6  ] ➸ GetEdgeValue     [ 13  ] ➸ PrintGraph          ▐" << endl;
    cout << "▐\t" << "[  7  ] ➸ GetNEdges                                      ▐" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << "▐\t" << "                            (  0  ) ➸ EXIT               ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " \n ➸ Choose an option:  ";
    cin >> option;

    return option;
}

